<?php
/**
 * @author Martin Kubovic
 */
require('header.php');
 try{
 
  if(!(isset($_GET['q']) and !empty($_GET['q']))){
    throw new Exception("Wrong GET parameters");
    }
    
  $base_url = "https://pokeapi.co/api/v2/pokemon/";
  $url_to_poke = $base_url.$_GET['q'];
  
  if(empty(@$content_json = file_get_contents($url_to_poke))) {
     throw new Exception("Cant get content from source");
    }
  
  if(empty($results = json_decode($content_json, true))){
    throw new Exception("Cant decode json");
    }  
    ?>
    <div class="container-fluid">  
      <div class="row">  
        <div class="col-sm-2"> 
          <h1><?php echo ucfirst($results['name']);?></h1>
        </div>
        <div class="col-sm-10"> 
          <img src="<?php echo $results['sprites']['front_default'];?>" border="0" class="img-circle">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-2">
          <b>Height:</b> <span class="text-info"><?php echo $results['height'];?></span>
        </div> 
        <div class="col-sm-2">
          <b>Weight:</b> <span class="text-info"><?php echo $results['weight'];?></span>
        </div>
        <div class="col-sm-2">
          <b>Species:</b> <span class="text-info"><?php echo $results['species']['name'];?></span>
        </div>  
      </div> 
              
         <b>Abilities:</b>
         <ul>
        <?php
            foreach ($results['abilities'] as $ability) {
              echo "<li>".$ability['ability']['name']."</li>";
            }
        ?>
        </ul>   
      </div>
      <?php 
 } catch (Exception $e) {
  ?>
    <div align=center>
    <h2>Your pokemon was not found :( Try it again!</h2>
    <br>
    <form method="get" action="pokemons.php">
      <label for="q">Pokémon:</label>
      <input type="search" id="q" name="q">
      <input type="submit" value="Search">
    </form>
    <a href="pokemons.php">Show me all pokemons!</a>
    </div>
  <?php
 }

 require('footer.php');
?>


        
        
        
    

