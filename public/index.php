<?php
/**
 * @author Martin Kubovic
 */
 require('header.php');
?>
        <div align="center" class="container-fluid">        
          <h1>Welcome adventurer!</h1>
          <br>
          <h3>What pokemon would you like to find today? </h3>
          
          <div align="center" style="padding-top:20px">        
            <form method="get" action="pokemons.php">
              <input type="search" id="q" name="q">
              <input type="submit" value="Search">
            </form>
          </div>
          
          <a href="pokemons.php">Show me all pokemons!</a>
        </div>
 
<?php
  require('footer.php');
?>