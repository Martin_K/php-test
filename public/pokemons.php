<?php
 /**
 * @author Martin Kubovic
 */
 require("header.php"); 
  $base_url = "https://pokeapi.co/api/v2/pokemon/";
  $url_to_list = "https://pokeapi.co/api/v2/pokemon/";
  if(isset($_GET['limit']) and isset($_GET['offset'])){
     $url_to_list = $url_to_list."?limit=".$_GET['limit']."&offset=".$_GET['offset'];
  } 
  if(isset($_GET['q']) and !empty($_GET['q'])){
    /*fetch minimal result for pokemon count*/
    $content_json = file_get_contents($base_url."?limit=1");
    $results = json_decode($content_json, true);
    /*fetch all pokemons*/
    $content_json = file_get_contents($base_url."?limit=".$results['count']);
    $results = json_decode($content_json, true);
    
    $pokemons_array = array();
    foreach($results['results'] as $pokemon){
        if(stripos($pokemon['name'], $_GET['q']) !== FALSE){
          array_push($pokemons_array, $pokemon);
        }
    }
  }
  else {
    $content_json = file_get_contents($url_to_list);
    $results = json_decode($content_json, true);
    $pokemons_array = $results['results'];
  }           
  
  if(isset($results['previous'])){
    $query_str = parse_url($results['previous'], PHP_URL_QUERY);
    $previous_set_link = "pokemons.php?".$query_str;
  }
   
  if(isset($results['next'])){
    $query_str = parse_url($results['next'], PHP_URL_QUERY);
    $next_set_link = "pokemons.php?".$query_str;
  }
      
  echo "<div class='container-fluid'>";
  echo "<h1>Pokemons</h1>";
  echo "<div class='list-group'>";
    foreach ($pokemons_array as $name) {
      echo "<a href='pokemon.php?q=" .$name['name']. "' class='list-group-item list-group-item-action list-group-item-dark'>" .ucfirst($name['name'])."</a>";
    }
  echo "</div>";

  echo "<ul class='pagination'>";
   if(!empty($results['previous'])){
      echo "<li class='page-item'><a href='".$previous_set_link."' class='page-link'>Previous</a></li>";
   }
   if(!empty($results['next'])){
      echo "<li class='page-item'><a href='".$next_set_link."' class='page-link'>Next</a></li>";
   }
  echo "</ul>";
  echo "</div>";

require("footer.php");  
?>

        
        